public interface LunarWidgets.Interface.Widget : Gtk.Widget {
	
	public abstract string get_widget_id();
	public abstract string? command(string command, string? data);
	public virtual bool add_widget(string position, Gtk.Widget widget) { return false; }
	
}
