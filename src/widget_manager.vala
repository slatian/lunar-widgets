public class LunarWidgets.WidgetManager : LunarWidgets.Interface.WidgetManager, Object {
	
	private HashTable<string, LunarWidgets.Interface.Widget> widgets = new HashTable<string, LunarWidgets.Interface.Widget>(str_hash, str_equal);
	
	private static void remove_widget_from_parent(Gtk.Widget widget) {
		var parent = widget.parent;
		if (parent != null) {
			parent.remove(widget);
		}
	}
	
	  //////////////////////////////////////////
	 // LunarWidgets.Interface.WidgetManager //
	//////////////////////////////////////////
	
	public LunarWidgets.Interface.Widget? get_widget(string widget_id){
		return widgets.get(widget_id);
	}
	
	public bool add_widget(LunarWidgets.Interface.Widget widget){
		var predecessor = get_widget(widget.get_widget_id());
		if (predecessor != null) {
			remove_widget_from_parent(predecessor);
		}
		widgets.set(widget.get_widget_id(), widget);
		return true;
	}
	
	public bool remove_widget(string widget_id){
		var widget = get_widget(widget_id);
		if (widget != null) {
			remove_widget_from_parent(widget);
			widgets.remove(widget_id);
			return true;
		}
		return false;
	}
	
	public bool unparent_widget(string widget_id){
		var widget = get_widget(widget_id);
		if (widget == null) { return false; }
		remove_widget_from_parent(widget);
		return true;
	}
	
	public bool assign_widget(string widget_id, string parent_id, string position){
		var parent = get_widget(parent_id);
		if (parent == null) { return false; }
		var widget = get_widget(widget_id);
		if (widget == null) { return false; }
		return parent.add_widget(position, widget);
	}
	
	public string? try_execute_command(string widget_id, string command, string? data){
		var widget = get_widget(widget_id);
		if (widget == null) { return null; }
		return widget.command(command, data);
	}
	
}
