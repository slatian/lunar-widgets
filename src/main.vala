
public static void message(string message) {
	stderr.write(message.data);
}

public static int main(string[] args) {

	// initialize gtk
	Gtk.init (ref args);
	
	//initialize the widget infrastructure
	var wm = new LunarWidgets.WidgetManager();
	//factory = new LunarWidgets.WidgetFactory(wm);
	
	//initialize a lua vm wich has access to all lua librarys
	var luavm = new Lua.LuaVM();
	luavm.open_libs();
	
	//Register quit function
	luavm.register("quit", quit);
	
	//register uuid function
	luavm.register("uuid", uuid);
	
	//initialize a bridge between the lua vm and the widget infrastructure
	LunarWidgets.LuaIntegration.WidgetManager.initialize(luavm, wm);
	luavm.register("timeout", timeout);
	
	//give lua some access to stdio that has no bugs
	luavm.register("output", lua_output);
	luavm.register("message", lua_message);
	
	
	// parse arguments
	string[] scriptfiles = {};
	
	bool next_is_scriptfile = false;
	bool next_is_css_file = false;
	bool print_help = false;
	string? firstarg = null;
	
	foreach(string arg in args) {
		if (firstarg == null) {
			firstarg = arg;
		} else if (next_is_scriptfile) {
			scriptfiles += arg;
			next_is_scriptfile = false;
		} else if (next_is_css_file) {
			message(@"[css] Loading css file: $arg\n");
			var error = LunarWidgets.GlobalCssLoader.add_global_css_provider(arg);
			if (error != null) {
				message(@"[css] Error while loading css file:\n $error\n");
			}
			next_is_css_file = false;
		} else {
			switch (arg) {
				case "-s":
				case "--scriptfile":
					next_is_scriptfile = true;
					break;
				case "--css":
					next_is_css_file = true;
					break;
				default:	
					print_help = true;
					break;
			}
		}
	}
	
	// print help
	if (print_help || scriptfiles.length == 0) {
		message(@"Usage: $firstarg -s <scriptfile>
Allows the creation of custom gtk based virtuel keyboard with a lua backend.

  -s, --scriptfile  the next argument is the path to the backend lua script
      --css         the next argument is a path to a css file (gtk css)

Example:
  $firstarg -s test.lua  Use the script test.lua as the backend
		  	
WARNING: The lua script runs in the same thread as the ui, if the script hangs, the whole application hangs!\n");
		return 0;
	}
	
	foreach (string scriptfile in scriptfiles) {
		message("[lua] Executing scriptfile: "+scriptfile+"\n");
		// execute lua script
		int res = luavm.load_file(scriptfile);
		if (res != 0) {
			message("[Lua][ERROR][loadfile] "+luavm.to_string(1)+"\n");
			luavm.pop(1);
			return res;
		}
		res = luavm.pcall(0,0,0); //0 arg, 0 ret, original error object
		if (res != 0) {
			message("[Lua][ERROR][init] "+luavm.to_string(1)+"\n");
			luavm.pop(1);
		}
	}
	
	message("[lua] Script successfully executed, running main loop\n");
	readloop.begin(new DataInputStream(new UnixInputStream(stdin.fileno(), false)), luavm);
	
	Gtk.main();
	return 0;
	
}

public static async void readloop(GLib.DataInputStream stream, Lua.LuaVM luavm) {
	while (true) {
		try {
			string? input = yield stream.read_line_utf8_async();
			if (input != null) {
				luavm.get_global("on_stdin_line");
				if (luavm.is_nil(-1)) {
					luavm.pop(1);
					return;
				}
				luavm.push_string(input);
				var res = luavm.pcall(1,0,0); //3 arg, 0 ret, original error object
				if (res != 0) {
					message("[Lua][ERROR][readloop()] "+luavm.to_string(1)+"\n");
					luavm.pop(1);
				}
			}
		} catch (Error e) {
			message(@"[input][error] $(e.message)");
		}
	}
}

public static int uuid(Lua.LuaVM luavm) {
	luavm.push_string(GLib.Uuid.string_random());
	return 1;
}

public static int quit(Lua.LuaVM luavm) {
	Gtk.main_quit();
	return 0;
}

public static int timeout(Lua.LuaVM luavm){
	string function = luavm.to_string(1);
	double millis = luavm.to_number(2)*1000;
	luavm.pop(2);
	Timeout.add((uint) millis ,() => {
		luavm.get_global(function);
		var res = luavm.pcall(0,0,0); //0 arg, 0 ret, original error object
		if (res != 0) {
			message("[Lua][ERROR][timeout()] "+luavm.to_string(1)+"\n");
			luavm.pop(1);
		}
		return false;
	},Priority.DEFAULT);
	return 0;
}

public static int lua_output(Lua.LuaVM luavm){
	print(luavm.to_string(1));
	luavm.pop(1);
	return 0;
}

public static int lua_message(Lua.LuaVM luavm){
	message(luavm.to_string(1)+"\n");
	luavm.pop(1);
	return 0;
}

