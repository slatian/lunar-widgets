public class LunarWidgets.Widget.Stack : Gtk.Stack, LunarWidgets.Interface.Widget {
	
	public string id;
	public LunarWidgets.Interface.WidgetManager wm;
	
	public Stack(LunarWidgets.Interface.WidgetManager wm, string id) {
		this.wm = wm;
		this.id = id;
	}
	
	  ///////////////////////////////////
	 // LunarWidgets.Interface.Widget //
	///////////////////////////////////
	
	public string get_widget_id(){
		return this.id;
	}
	
	public bool add_widget(string position, Gtk.Widget widget){
		this.add_named(widget, position);
		return true;
	}
	
	public string? command(string command, string? data) {
		string? result;
		result = LunarWidgets.CommandHelper.execute_generic_widget_command(command, data, this);
		if (result != null) { return result; }
		switch(command) {
			case "homogeneous":
				if (data != null) {
					this.homogeneous = LunarWidgets.CommandHelper.string_to_bool(data);
				}
				return LunarWidgets.CommandHelper.bool_to_string(this.homogeneous);
			case "hhomogeneous":
				if (data != null) {
					this.hhomogeneous = LunarWidgets.CommandHelper.string_to_bool(data);
				}
				return LunarWidgets.CommandHelper.bool_to_string(this.hhomogeneous);
			case "vhomogeneous":
				if (data != null) {
					this.vhomogeneous = LunarWidgets.CommandHelper.string_to_bool(data);
				}
				return LunarWidgets.CommandHelper.bool_to_string(this.vhomogeneous);
			case "visible_child_name":
				if (data != null) {
					this.visible_child_name = data;
				}
				return this.visible_child_name;
			case "interpolate_size":
				if (data != null) {
					this.interpolate_size = LunarWidgets.CommandHelper.string_to_bool(data);
				}
				return LunarWidgets.CommandHelper.bool_to_string(this.interpolate_size);
			case "transition_duration":
				if (data != null) {
					this.transition_duration = LunarWidgets.CommandHelper.parse_integer(data, 0);
				}
				return @"$transition_duration";
			case "transition_type":
				if (data != null) {
					switch (data) {
						case "none":
							this.transition_type = NONE;
							break;
						case "over_down":
							this.transition_type = OVER_DOWN;
							break;
						case "over_down_up":
							this.transition_type = OVER_DOWN_UP;
							break;
						case "over_left":
							this.transition_type = OVER_LEFT;
							break;
						case "over_left_right":
							this.transition_type = OVER_LEFT_RIGHT;
							break;
						case "over_right":
							this.transition_type = OVER_RIGHT;
							break;
						case "over_right_left":
							this.transition_type = OVER_RIGHT_LEFT;
							break;
						case "over_up":
							this.transition_type = OVER_UP;
							break;
						case "over_up_down":
							this.transition_type = OVER_UP_DOWN;
							break;
						case "slide_down":
							this.transition_type = SLIDE_DOWN;
							break;
						case "slide_left":
							this.transition_type = SLIDE_LEFT;
							break;
						case "slide_left_right":
							this.transition_type = SLIDE_LEFT_RIGHT;
							break;
						case "slide_right":
							this.transition_type = SLIDE_RIGHT;
							break;
						case "slide_up":
							this.transition_type = SLIDE_UP;
							break;
						case "slide_up_down":
							this.transition_type = SLIDE_UP_DOWN;
							break;
						case "under_down":
							this.transition_type = UNDER_DOWN;
							break;
						case "under_left":
							this.transition_type = UNDER_LEFT;
							break;
						case "under_right":
							this.transition_type = UNDER_RIGHT;
							break;
						case "under_up":
							this.transition_type = UNDER_UP;
							break;
						case "crossfade":
						default:
							this.transition_type = CROSSFADE;
							break;
					}
				}
				return "unknown";
			default:
				return null;
		}
	}
	
}
