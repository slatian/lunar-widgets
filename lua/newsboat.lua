make_widget("window", "main_window")
widget_command("main_window","accept_focus","false")
widget_command("main_window","make_dock")

local function type_key(key)
	os.execute("xdotool keydown "..key.." keyup "..key)
end

--callbacks[widget_id][signal_name] = function
local callbacks = {}

local function make_keyboard(id, parent_id, position, description, icon_size)
	make_widget("box", id, parent_id, position)
	widget_command(id, "orientation", "v")
	widget_command(id, "homogeneous", "yes")
	for i = 1,#description do
		local row_id = id.."."..i
		local row = description[i]
		make_widget("box", row_id, id, "start")
		widget_command(row_id, "orientation", "h")
		widget_command(row_id, "homogeneous", "yes")
		for j = 1,#row do
			local key = row[j]
			local key_id = row_id.."."..j
			make_widget("button", key_id, row_id, "start")
			if key.label then
				widget_command(key_id, "label", key.label)
			end
			if key.icon then
				local icon_id = key_id..".icon"
				make_widget("image", icon_id, key_id)
				widget_command(icon_id, "icon", key.icon)
				if icon_size then
					widget_command(icon_id, "size", icon_size)
				end
			end
			if key.class then
				widget_command(key_id, "add_style_class", key.class)
			end
			if key.cmd then
				if not callbacks[key_id] then
					callbacks[key_id] = {}
				end
				callbacks[key_id]["clicked"] = key.cmd
			end
		end
	end
end

local function callback(f,...)
	local args = table.pack(...)
	return function ()
		f(table.unpack(args))
	end
end

local keyboard = {
	{
		{icon="go-previous-symbolic", cmd=callback(type_key, "q"), class="test"},
		{label="open", cmd=callback(type_key, "o")},
		{icon="go-up-symbolic", cmd=callback(type_key, "Up")},
		{icon="go-jump-symbolic", cmd=callback(type_key, "Return"), class="suggested-action"},
		{icon="starred-symbolic", cmd=callback(type_key, "N")},
	},{
		{icon="view-refresh-symbolic", cmd=callback(type_key, "r")},
		{label="urls", cmd=callback(type_key, "o")},
		{icon="go-down-symbolic", cmd=callback(type_key, "Down")},
		{icon="input-keyboard-symbolic", cmd=quit},
		{icon="list-remove-all-symbolic", cmd=callback(type_key, "A")}
	}
}



make_keyboard("main_keyboard","main_window", nil, keyboard, 32)

function on_widget_signal(source_id, signal, data)
	print("Received signal:", source_id, signal, data)
	if source_id == "main_window" and signal == "destroy" then
		print("Quitting…")
		quit()
		return
	end
	
	local widget_callbacks = callbacks[source_id]
	if widget_callbacks then
		local callback = widget_callbacks[signal]
		if callback then
			callback(data)
			return
		end
	end
end
