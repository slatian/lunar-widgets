
-- disable debug messages
local function message() end

local function proxy_object_set_property(this, key, value)
	this.connection:set_property(this.name, key, value)
end

local function proxy_object_request_property(this, key)
	this.connection:request_property(this.name, key)
end

local function proxy_object_send_signal(this, key, value)
	this.connection:send_signal(this.name, key, value)
end

local function proxy_object_subscribe(this, controller)
	this._controllers[controller] = true
end

local function proxy_object_unsubscribe(this, controller)
	this._controllers[controller] = nil
end

local function proxy_object_on_property_update(this, key, value)
	message("Proxy object received property update "..key.." = "..value)
	for controller,_ in pairs(this._controllers) do
		message("Trying to notify controller "..tostring(controller))
		if controller.on_property_update then
			message("Calling controller")
			controller:on_property_update(this, key, value)
		end
	end
end

local function proxy_object_on_signal(this, signal_name)
	for controller,_ in pairs(this._controllers) do
		if controller.on_signal then
			controller:on_signal(this, signal_name)
		end
	end
end

local function proxy_object_gc(this)
	this.connection:unregister(this)
end

function ProxyObject(connection, name)
	local this = {
		connection = connection,
		name = name,
		set = proxy_object_set_property,
		request = proxy_object_request_property,
		signal = proxy_object_send_signal,
		subscribe = proxy_object_subscribe,
		unsubscribe = proxy_object_unsubscribe,
		on_property_update = proxy_object_on_property_update,
		on_signal = proxy_object_on_signal,
	}
	local controllers = {}
	setmetatable(controllers, { __mode = "k" })
	this._controllers = controllers
	
	connection:register(this)
	
	return this
end

-----------------------------------------------------------
--- Connection Template ----------------------------------
---------------------------------------------------------

local function connection_template_unimplemented()
	error("Tried to callan unimplemented function on a connection template!")
end

local function connection_template_register(this, proxy_object)
	this._proxy_objects[proxy_object.name] = proxy_object
	this:subscribe(proxy_object.name)
end

local function connection_template_unregister(this, proxy_object)
	this._proxy_objects[proxy_object.name] = nil
	this:unsubscribe(proxy_object.name)
end

local function connection_template_get_proxy_object(this, name)
	if not this._proxy_objects[name] then
		-- object will register itself
		ProxyObject(this, name)
	end
	return this._proxy_objects[name]
end

local function connection_template_on_property_update(this, object_name, key, value)
	message("Updating property for "..object_name..": "..key.." = "..value)
	local object = this._proxy_objects[object_name]
	if object then
		message("Found object")
		object:on_property_update(key, value)
	end
end

local function connection_template_on_signal(this, object_name, signal_name)
	local object = this._proxy_objects[object_name]
	if object then
		object:on_signal(signal_name)
	end
end

function ConnectionTemplate()
	this = {
		register = connection_template_register,
		unregister = unconnection_template_register,
		get_proxy_object = connection_template_get_proxy_object,
		on_property_update = connection_template_on_property_update,
		on_signal = connection_template_on_signal,
		set_property = connetion_template_unimplemented,
		request_property = connetion_template_unimplemented,
		send_signal = connetion_template_unimplemented,
		subscribe = connection_template_unimplemented,
		unsubscribe = connection_template_unimplemented,
	}
	
	local proxy_objects = {}
	setmetatable(proxy_objects, { __mode = "v" })
	this._proxy_objects = proxy_objects
	
	return this
end

-----------------------------------------------------------
--- DDB Connection ---------------------------------------
---------------------------------------------------------

local function ddb_connection_set_property(this, object_name, key, value)
	if value == nil then
		this.outstream:write(("u %s %s\n"):format(object_name, key))
	else
		this.outstream:write(("> %s %s %s\n"):format(object_name, key, value))
	end
end

local function ddb_connection_request_property(this, object_name, key)
	this.outstream:write(("r %s %s\n"):format(object_name, key))
end

local function ddb_connection_send_signal(this, object_name, signal_name)
	this.outstream:write(("s %s %s\n"):format(object_name, signal_name))
end

local function ddb_connection_subscribe(this, object_name)
	this.outstream:write(("+ %s\n"):format(object_name))
end

local function ddb_connection_unsubscribe(this, object_name)
	this.outstream:write(("- %s\n"):format(object_name))
end

local function ddb_connection_eval_line(this, line)
	local command, object_name, key, value = line:match("^(.) (.-) ([^ ]+) ?(.*)")
	if command == ">" then
		this:on_property_update(object_name, key, value)
	elseif command == "s" then
		this:on_signal(object_name, key)
	end
end

function DDBConnection(outstream, greeting)
	if greeting then
		outstream:write(greeting.."\n")
	end
	local this = ConnectionTemplate()
	this.outstream = outstream
	this.set_property = ddb_connection_set_property
	this.request_property = ddb_connection_request_property
	this.send_signal = ddb_connection_send_signal
	this.subscribe = ddb_connection_subscribe
	this.unsubscribe = ddb_connection_unsubscribe
	this.eval_line = ddb_connection_eval_line
	return this
end
