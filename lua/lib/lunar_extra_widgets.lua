local nm_connecting_animation = {
	"nm-stage01-connecting01",
	"nm-stage01-connecting02",
	"nm-stage01-connecting03",
	"nm-stage01-connecting04",
	"nm-stage01-connecting05",
	"nm-stage01-connecting06",
	"nm-stage01-connecting07",
	"nm-stage01-connecting08",
	"nm-stage01-connecting09",
	"nm-stage01-connecting10",
	"nm-stage01-connecting11",
}

function WifiButton(attr)
	attr.icon = "network-wireless-disconnected"
	attr._looks = {
		active     = { active=true, icon="network-wireless-connected" },
		active_00  = { active=true, icon="network-wireless-connected-00" },
		active_25  = { active=true, icon="network-wireless-connected-25" },
		active_50  = { active=true, icon="network-wireless-connected-50" },
		active_75  = { active=true, icon="network-wireless-connected-75" },
		active_100 = { active=true, icon="network-wireless-connected-100" },
		disconnected = { active=false, icon="network-wireless-disconnected" },
		connecting = { active=true, icon="network-wireless-acquiring", animation=nm_connecting_animation, fps=8},
		active_ap  = { active=true, icon="network-wireless-hotspot"},
		active_no_route = { active=true, icon="network-wireless-no-route"},
	}
	attr.state = "disconnected"
	return StateButton(attr)
end

function TorchButton(attr)
	attr.icon = "redshift-status-off"
	attr.state = "off"
	attr._looks = {
		off = { active=false, icon="redshift-status-off" },
		on = { active=true, icon="redshift-status-on" },
		unavailable = { active=false, icon="battery-000" },
	}
	return StateButton(attr)
end

function BluetoothButton(attr)
	attr.icon = "bluetooth"
	attr.state = "disabled"
	attr._looks = {
		disabled = { active=false, icon="bluetooth-disabled"},
		active = { active=true, icon="bluetooth-active"},
		paired = { active=true, icon="bluetooth-paired"},
	}
	return StateButton(attr)
end

function ServiceButton(attr)
	attr.state = "disabled"
	attr._looks = {
		active = { active=true, icon=(attr.icon_active or attr.icon)},
		inactive = { active=false, icon=(attr.icon_inactive or attr.icon)},
		notification = {active=true, icon=(attr.icon_notify or attr.icon_active or attr.icon)},
		-- for example for push notifications
		notification_inactive = {active=false, icon=(attr.icon_notify or attr.icon_inactive or attr.icon)}
	}
	local this = StateButton(attr)
	this:call("add_style_class", "service-button")
	return this
end

local function notification_widget_set_title(this, label)
	this._title_label.label = label
end

local function notification_widget_get_title(this)
	return this._title_label.label
end

local function notification_widget_set_icon(this, icon)
	if icon then
		this._title_icon.icon = icon
	else
		this._title_icon.visible = false
	end
end

local function notification_widget_get_icon(this)
	return this._title_icon.icon
end

local function notification_widget_set_icon_file(this, path)
	this._title_icon.visible = path ~= nil
	this._title_icon.file = path
end

local function notification_widget_get_icon_file(this)
	return this._title_icon.file
end

local function notification_widget_set_relevancy_label(this, label)
	this._relevancy_label.label = label
end

local function notification_widget_get_relevancy_label(this)
	return this._relevancy_label.label
end

local function notification_widget_set_description(this, description)
	if description and description ~= "" then
		this._description_label.label = description
		this._description_label.visible = true
	else
		this._description_label.visible = false
		this._description_label.label = nil
	end
end

local function notification_widget_get_description(this)
	return this._description_label.label
end

local function notification_widget_set_signal_close(this, fn)
	this._close_button._signal_clicked = fn
end

local function notification_widget_get_signal_close(this)
	return this._close_button._signal_clicked
end

local function notification_widget_set_has_close_button(this, visible)
	this._close_button.visible = visible
end

local function notification_widget_get_has_close_button(this)
	return this._close_button.visible
end

local function notification_widget_add_widget(this, widget, position)
	if position == "title-action" then
		this._titlebar:add_widget(widget, "end.no-expand.no-fill")
	else		
		this:_add_widget(widget, position)
	end
end

function NotificationWidget(attr)
	local title_icon = Image{
		class = "title-icon",
		icon = "flag-symbolic",
		visible = false,
	}
	local title_label = Label{
		label = attr.title,
		valign = "center",
		halign = "start",
		ellipsize = "end",
		single_line_mode = true,
	}
	-- relevancy_label for displaying progress or time issued
	local relevancy_label = Label{
		label = attr.relevancy_label or "",
		valign = "center",
		halign = "end",
		class = "relevancy-label",
		single_line_mode = true,
	}
	local close_button = Button{
		icon = "window-close",
		valign = "center",
		halign = "end",
	}
	local description_label = Label{
		label = attr.description,
		class = "notification-description",
		halign = "start",
		visible = false,
		wrap = true,
	}
	local titlebar = Box{
		orientation="h",
		homogeneous=false,
		halign = "fill",
		valign = "start",
		class = "notification-titlebar",
	}
	titlebar:add_widget(title_icon, "start.no-expand.no-fill")
	titlebar:add_widget(title_label)
	titlebar:add_widget(close_button, "end.no-expand.no-fill")
	titlebar:add_widget(relevancy_label, "end.no-expand.no-fill")
	attr.orientation = "v"
	attr.homogeneous = false
	local _signal_close = attr._signal_close
	attr._signal_close = nil
	local icon = attr.icon
	local icon_file = attr.icon_file
	attr.icon = nil
	attr.icon_file = nil
	local this = Box(attr)
	this:add_widget(titlebar, "start.no-expand.no-fill")
	this:add_widget(description_label)
	this._title_icon = title_icon
	-- TODO: find out why this sh…stuff is not working
	if icon then this._title_icon.icon = icon end
	if icon_file then this._title_icon.icon_file = icon_file end
	this._title_label = title_label
	this._relevancy_label = relevancy_label
	this._description_label = description_label
	this._close_button = close_button
	this._titlebar = titlebar
	this._set_title = notification_widget_set_title
	this._get_title = notification_widget_get_title
	this._set_icon = notification_widget_set_icon
	this._get_icon = notification_widget_get_icon
	this._set_icon_file = notification_widget_set_icon_file
	this._get_icon_file = notification_widget_get_icon_file
	this._set_relevancy_label = notification_widget_set_relevancy_label
	this._get_relevancy_label = notification_widget_get_relevancy_label
	this._set_description = notification_widget_set_description
	this._get_description = notification_widget_get_description
	this._set__signal_close = notification_widget_set_signal_close
	this._get__signal_close = notification_widget_get_signal_close
	this._signal_close = _signal_close
	this._set_has_close_button = notification_widget_set_has_close_button
	this._get_has_close_button = notification_widget_get_has_close_button
	this:call("add_style_class", "notification")
	this._add_widget = this.add_widget
	rawset(this, "add_widget", notification_widget_add_widget)
	this.margin = 0
	if attr.has_close_button == false then
		this._close_button.visible = false
	end
	this._title_icon.visible = false
	return this
end

local function notification_action_buttons_set_button_label(this, button_no, label)
	if label and label ~= "" then
		this:ensure_button(button_no)
		local button = this._buttons[button_no]
		button.visible = true
		button.label = label
		button._has_label = true
	else
		local button = this._buttons[button_no]
		if button then
			button.label = nil
			button._has_label = false
			button.visible = button._has_icon
		end
	end
end

local function notification_action_buttons_set_button_icon(this, button_no, icon)
	if icon and icon ~= "" then
		this:ensure_button(button_no)
		local button = this._buttons[button_no]
		button.visible = true
		button.icon = icon
		button._has_icon = true
	else
		local button = this._buttons[button_no]
		if button then
			button.icon = nil
			button._has_icon = false
			button.visible = button._has_label
		end
	end
end

local function notification_action_buttons_button_clicked_handeler(this)
	if this.parent then
		if this.parent._signal_action then
			this.parent._signal_action(this.parent, this._number)
		end
	end
end

local function notification_action_buttons_ensure_button(this, button_no)
	if button_no > 1 then
		this:ensure_button(button_no-1)
	end
	if (not this._buttons[button_no]) then
		this._buttons[button_no] = Button{
			parent = this,
			position = "start",
			_has_label = false,
			_has_icon = false,
			_number = button_no,
			_signal_clicked = notification_action_buttons_button_clicked_handeler,
		}
	end
end

function NotificationActionButtons(attr)
	local this = Box(attr)
	this._buttons = {}
	rawset(this, "ensure_button", notification_action_buttons_ensure_button)
	rawset(this, "set_label", notification_action_buttons_set_button_label)
	rawset(this, "set_icon", notification_action_buttons_set_button_icon)
	return this
end

local function media_player_controls_update_buttons(this)
	this._seek_back.visible = this._can_seek
	this._seek_forward.visible = this._can_seek
	this._skip_back.sensitive = this._can_skip_back
	this._skip_forward.sensitive = this._can_skip_forward
end

function MediaPlayerControls(attr)
	local this = Box(attr)
	rawset(this, "update_buttons", media_player_controls_update_buttons)
	this._play_pause = StateButton{
		icon = "media-playback-start-symbolic",
		icon_size = attr.icon_size,
		_looks = {
			playing = { active=true, icon="media-playback-pause-symbolic" },
			paused = { active=false, icon="media-playback-start-symbolic"},
		},
		state = "paused",
	}
	this._seek_back = Button{
		icon = "media-seek-backward-symbolic",
		icon_size = attr.icon_size,
	}
	this._seek_forward = Button{
		icon = "media-seek-forward-symbolic",
		icon_size = attr.icon_size,
	}
	this._skip_back = Button{
		icon = "media-skip-backward-symbolic",
		icon_size = attr.icon_size,
	}
	this._skip_forward = Button{
		icon = "media-skip-forward-symbolic",
		icon_size = attr.icon_size,
	}
	this:add_widget(this._skip_back)
	this:add_widget(this._seek_back)
	this:add_widget(this._play_pause)
	this:add_widget(this._seek_forward)
	this:add_widget(this._skip_forward)
	return this
end

local function media_player_widget_set_playing(this, playing)
	if this._is_playing ~= playing then
		this._is_playing = playing
		if playing then
			this._controls._play_pause.state = "playing"
		else
			this._controls._play_pause.state = "paused"
		end
	end
end

local function media_player_widget_get_playing(this)
	return this._is_playing
end

local function seconds_to_string(seconds)
	if not seconds then return nil end
	local minutes = seconds//60
	local hours = minutes//60
	return (hours > 0 and (tostring(hours)..":"))..tostring(minutes%60)..":"..tostring(math.floor(seconds%60))
end

local function media_player_widget_set_current_play_time(this, seconds)
	this._current_play_time = seconds
	this:update_progress_label()
end

local function media_player_widget_update_progress_label(this)
	this._progress_label.label = (seconds_to_string(this._current_play_time) or "-").." / "..(seconds_to_string(this._total_time) or "-")
end

function MediaPlayerWidget(attr)
	local this = NotificationWidget(attr)
	this._set_current_play_time = media_player_widget_set_current_play_time
	this._set_playing = media_player_widget_set_playing
	this._get_playing = media_player_widget_get_playing
	
	this._controls = MediaPlayerControls{
		parent = this,
	}

	this._progress_label = Label{
		parent = this,
		position = "title-action",
		label = "- / -",
	}

	return this
end
